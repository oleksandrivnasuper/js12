// Теоретичні питання
// 1.Чому для роботи з input не рекомендується використовувати клавіатуру?
//Тому, що при використанні поля инпут треба брати до уваги й інші способи вводу,щоб правильно відслідкувати введе, наприклад мишкою або голосовію

// Завдання
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.


const btnEnter = document.querySelector('#enter');
const btnS = document.querySelector('#s');
const btnE = document.querySelector('#e');
const btnO = document.querySelector('#o');
const btnN = document.querySelector('#n');
const btnL = document.querySelector('#l');
const btnZ = document.querySelector('#z');

document.body.addEventListener('keyup', (e) => {
    if (e.key === 'Enter'){
        
        console.log(e.key);
        btnEnter.focus();
    }
    if (e.key === 's'){
        
        console.log(e.key);
        btnS.focus();
    } 
    if (e.key === 'e'){
        
        console.log(e.key);
        btnE.focus();
    } 
    if (e.key === 'o'){
        
        console.log(e.key);
        btnO.focus();
    } 
    if (e.key === 'n'){
        
        console.log(e.key);
        btnN.focus();
    } 
    if (e.key === 'l'){
        
        console.log(e.key);
        btnL.focus();
    } 
    if (e.key === 'z'){
        
        console.log(e.key);
        btnZ.focus();
    }
})

